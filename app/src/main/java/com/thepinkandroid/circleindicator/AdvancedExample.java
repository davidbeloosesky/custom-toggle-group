package com.thepinkandroid.circleindicator;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.Toast;
import android.widget.ToggleButton;

public class AdvancedExample extends AppCompatActivity {
    private static Context msContext;
    private static ViewPager mViewPager;
    private CustomPagerAdapter mCustomPagerAdapter;
    private RadioGroup mToggleGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advanced_example);

        msContext = this;
        setViewPager();
        setToggleGroup();
    }

    private void setViewPager() {
        mViewPager = findViewById(R.id.imageViewPager);
        mCustomPagerAdapter = new CustomPagerAdapter(this);
        mViewPager.setAdapter(mCustomPagerAdapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int position) {
                Toast.makeText(getApplicationContext(), "Selected position" + position, Toast.LENGTH_SHORT).show();
                for (int j = 0; j < mToggleGroup.getChildCount(); j++) {
                    ToggleButton toggleButton = (ToggleButton) mToggleGroup.getChildAt(j);
                    toggleButton.setChecked(position == j);
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    private void setToggleGroup() {
        mToggleGroup = findViewById(R.id.toggleGroup);
        mToggleGroup.setOnCheckedChangeListener(ToggleGroupListener);
    }

    static final RadioGroup.OnCheckedChangeListener ToggleGroupListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(final RadioGroup radioGroup, final int i) {
            for (int j = 0; j < radioGroup.getChildCount(); j++) {
                ToggleButton toggleButton = (ToggleButton) radioGroup.getChildAt(j);
                toggleButton.setChecked(toggleButton.getId() == i);
                if(toggleButton.getId() == i)
                {
                    mViewPager.setCurrentItem (j, true);
                    Toast.makeText(msContext, "Selected position : " + j, Toast.LENGTH_SHORT).show();
                }
            }
        }
    };


    public void onToggle(View view) {
        ((RadioGroup)view.getParent()).check(view.getId());
    }
}
