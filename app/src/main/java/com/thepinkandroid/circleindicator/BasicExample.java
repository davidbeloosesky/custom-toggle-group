package com.thepinkandroid.circleindicator;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.Toast;
import android.widget.ToggleButton;

public class BasicExample extends AppCompatActivity {
    private static Context msContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic_example);

        msContext = this;
        setToggleGroup();
    }

    private void setToggleGroup() {
        RadioGroup toggleGroup = findViewById(R.id.toggleGroup);
        toggleGroup.setOnCheckedChangeListener(ToggleGroupListener);
    }

    static final RadioGroup.OnCheckedChangeListener ToggleGroupListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(final RadioGroup radioGroup, final int i) {
            for (int j = 0; j < radioGroup.getChildCount(); j++) {
                final ToggleButton view = (ToggleButton) radioGroup.getChildAt(j);
                view.setChecked(view.getId() == i);
                if(view.getId() == i)
                {
                    Toast.makeText(msContext, "Selected position : " + j, Toast.LENGTH_SHORT).show();
                }
            }
        }
    };


    public void onToggle(View view) {
        ((RadioGroup)view.getParent()).check(view.getId());
    }
}
