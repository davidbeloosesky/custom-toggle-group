package com.thepinkandroid.circleindicator;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class CustomPagerAdapter extends PagerAdapter {

    private int[] mImagesArray = {R.drawable.redmixerimage, R.drawable.yellowmixerimage, R.drawable.brownmixerimage, R.drawable.cyanmixerimage};
    private Context mContext;
    private LayoutInflater mLayoutInflater;

    public CustomPagerAdapter(Context context) {
        mContext = context;
    }

    @Override
    public int getCount() {
        return mImagesArray.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = mLayoutInflater.inflate(R.layout.swipe_element, container, false);
        ImageView imageView = itemView.findViewById(R.id.imageViewItem);
        imageView.setImageResource(mImagesArray[position]);
        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((ConstraintLayout)object);
    }
}
