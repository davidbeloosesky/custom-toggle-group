package com.thepinkandroid.circleindicator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setButtons();
    }

    private void setButtons() {
        setBasicExample();
        setAdvancedExample();
    }

    private void setBasicExample() {
        findViewById(R.id.basicExampleBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), BasicExample.class));
            }
        });
    }
    private void setAdvancedExample() {
        findViewById(R.id.advancedExampleBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), AdvancedExample.class));
            }
        });
    }
}
